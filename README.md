# Indesign Script to Generate a Colour Search Pattern

This is a script to generate a colour search pattern using Indesign.

![Indesign Screenshot](img/id_screenshot.png)

## Script Installation

Download the complete [`SearchPatternGenerator.jsx` Repository](https://bitbucket.org/nickheaphy/indesign-colour-search-pattern-generator/get/HEAD.zip) as a ZIP.

Open Indesign, then the Indesign Scripts window (Window > Utilities > Scripts). Locate the folder called "User" and right click on it and select the "Revel In...". This will open Finder (Mac) or Explorer (PC) at the correct location.

Extract the ZIP downloaded above and copy the [`SearchPatternGenerator.jsx`](https://bitbucket.org/nickheaphy/indesign-colour-search-pattern-generator/raw/HEAD/SearchPatternGenerator.jsx) file into this folder.

## Script Options

When you run the script you will be presented with the options dialog

![Options Dialog](img/options_screenshot.png)

You can choose to make a search pattern using either CMYK or RGB colourspace.

You then can enter the starting colour for the search. If you are generating a CMYK search pattern you can enter the maximum TAC if your printing process requires one.

When generating the chart you can choose what is adjusted on both the horizontal and vertical axis, as well as how much variation there is along the axis.

Eg the default is that on the horizontal axis, the hue of the colour changes, while on the vertical axis the saturation of the colour changes. The variance level determines the amount of change - the higher the number, the more change there is.

Size determines the size of each of the colour swatches.

Gap determines the amount of space between the colour swatches (eg you might need a larger gap between dark colours on an inkjet device if you are reaching the papers maximum ink saturation level)

When the page is generated you will see three search grids. The Step Variance determines the left and right search grids adjustment amount (ie using the default horizontal=hue, vertical=saturation, the left search grid would have the brightness decreased, while the right search grid would have the brightness increased. The Step Variance determines the amount of this brightness adjustment)

The Page settings determines the size of the output page.

## Behind the scenes

The CMYK/RGB colour is converted to HSB colour, then, depending on the selections in the options, the hue, saturation and brightness values are shifted, proportionate to the x,y position on the page, then the colour is converted back to the original colourspace. Note this does have the unfortunate side effect for CMYK of remixing the black channel, so the documents default profile does come into play.

## Note

If you are converting the Indesign file to a PDF before printing, please ensure that you use the appropriate colour management settings when generating the PDF to avoid Indesign making changes to the colours of the file. The PDF Export preset 'High Quality Print' is a reasonable choice as the Output > Color Conversion is set to 'No Color Conversion'

### Version History

v1.0 - 12/9/2021 - Initial Version

v1.1 - 6/10/2022 - Added support for Parent Pages