// An Indesign Userscript to generate a colour search pattern

var scriptname = "SearchPatternGenerator.js";
var scriptversion = "v1.1";

main();

function main() {
    mySetup();
    myCode();
}

function mySetup() {
    //Make certain that user interaction (display of dialogs, etc.) is turned on.
    app.scriptPreferences.userInteractionLevel = UserInteractionLevels.interactWithAll;
}

function myCode() {
    //build the dialog
    var myDialog = app.dialogs.add({name:"Colour Search Pattern Generator "+scriptversion, canCancel:true});
    var myLabelWidth = 90;
    var myTitleWidth = 100;
    with (myDialog) {
        with (dialogColumns.add()) {
            with (dialogRows.add()) {
                with (dialogColumns.add()) {
                    var myCMYK = enablingGroups.add({staticLabel:"CMYK Colour", checkedState:true});
                    with (myCMYK) {
                        with(borderPanels.add()){
                            with(dialogColumns.add()){
                                staticTexts.add({staticLabel:"Cyan:", minWidth:myLabelWidth});
                                staticTexts.add({staticLabel:"Magenta:", minWidth:myLabelWidth});
                                staticTexts.add({staticLabel:"Yellow:", minWidth:myLabelWidth});
                                staticTexts.add({staticLabel:"Black:", minWidth:myLabelWidth});
                                staticTexts.add({staticLabel:"Max TAC:", minWidth:myLabelWidth});
                            }
                            with(dialogColumns.add()){
                                var cyanValField = integerEditboxes.add({editValue:0,maximumValue:100,minimumValue:0});
                                var magentaValField = integerEditboxes.add({editValue:100,maximumValue:100,minimumValue:0});
                                var yellowValField = integerEditboxes.add({editValue:100,maximumValue:100,minimumValue:0});
                                var blackValField = integerEditboxes.add({editValue:5,maximumValue:100,minimumValue:0});
                                var tacValField = integerEditboxes.add({editValue:400,maximumValue:400,minimumValue:0});
                            }
                            with(dialogColumns.add()){
                                staticTexts.add({staticLabel:"%"});
                                staticTexts.add({staticLabel:"%"});
                                staticTexts.add({staticLabel:"%"});
                                staticTexts.add({staticLabel:"%"});
                                staticTexts.add({staticLabel:"%"});
                            }
                        }
                    }
                }
                with (dialogColumns.add()) {
                    var myRGB = enablingGroups.add({staticLabel:"RGB Colour", checkedState:false});
                    with (myRGB) {
                        with(borderPanels.add()){
                            with(dialogColumns.add()){
                                staticTexts.add({staticLabel:"Red:", minWidth:myLabelWidth});
                                staticTexts.add({staticLabel:"Green:", minWidth:myLabelWidth});
                                staticTexts.add({staticLabel:"Blue:", minWidth:myLabelWidth});
                            }
                            with(dialogColumns.add()){
                                var redValField = integerEditboxes.add({editValue:204,maximumValue:255,minimumValue:0});
                                var greenValField = integerEditboxes.add({editValue:0,maximumValue:255,minimumValue:0});
                                var blueValField = integerEditboxes.add({editValue:0,maximumValue:255,minimumValue:0});
                            }
                        }
                    }
                }
            }
            with (dialogRows.add()) {
                with(dialogColumns.add()){
            
                    with(borderPanels.add()){
                        staticTexts.add({staticLabel:"Chart Generation", minWidth:myTitleWidth});
                        with(dialogColumns.add()){
                            staticTexts.add({staticLabel:"", minWidth:myLabelWidth});
                            staticTexts.add({staticLabel:"Horizontal:", minWidth:myLabelWidth});
                            staticTexts.add({staticLabel:"Vertical:", minWidth:myLabelWidth});
                            staticTexts.add({staticLabel:"Size (mm):", minWidth:myLabelWidth});
                            staticTexts.add({staticLabel:"Gap (mm):", minWidth:myLabelWidth});
                            staticTexts.add({staticLabel:"Step Variance:", minWidth:myLabelWidth});
                        }
                        with(dialogColumns.add()){
                            with(dialogRows.add()) staticTexts.add({staticLabel:"Adjustment"});
                            var myHoriColMenu = dropdowns.add({stringList:["Hue", "Saturation", "Lightness"], selectedIndex:0});
                            var myVertColMenu = dropdowns.add({stringList:["Hue", "Saturation", "Lightness"], selectedIndex:1});
                            var myBoxSizeField = integerEditboxes.add({editValue:20,minimumValue:10,maximumValue:100});
                            var myGapField = integerEditboxes.add({editValue:0});
                            var myPageStepField = dropdowns.add({stringList:["1", "2", "3", "4", "5", "6"], selectedIndex:2});
                        }
                        with(dialogColumns.add()){
                            with(dialogRows.add()) staticTexts.add({staticLabel:"Variance"});
                            var myHoriStepField = dropdowns.add({stringList:["1", "2", "3", "4", "5", "6"], selectedIndex:2});
                            var myVertStepField = dropdowns.add({stringList:["1", "2", "3", "4", "5", "6"], selectedIndex:2});
                        }
                    }
                    with(borderPanels.add()){
                        staticTexts.add({staticLabel:"Page", minWidth:myTitleWidth});
                        with(dialogColumns.add()){
                            staticTexts.add({staticLabel:"Width:", minWidth:myLabelWidth});
                            staticTexts.add({staticLabel:"Height:", minWidth:myLabelWidth});
                            //staticTexts.add({staticLabel:"Steps:", minWidth:myLabelWidth});
                            //staticTexts.add({staticLabel:"Variance:", minWidth:myLabelWidth});
                        }
                        with(dialogColumns.add()){
                            var myPageWidthField = measurementEditboxes.add({editValue:1190.5504,editUnits:MeasurementUnits.MILLIMETERS,minWidth:myLabelWidth});
                            var myPageHeightField = measurementEditboxes.add({editValue:841.8892, editUnits:MeasurementUnits.MILLIMETERS,minWidth:myLabelWidth});
                            //var myTotalPagesField = integerEditboxes.add({editValue:1,minimumValue:1,maximumValue:999});
                        }
                    }
                }
            }
        }
    }


    // display the dialog
    if (myDialog.show() == true) {
        // check the user has not entered anything weird
        if ((myCMYK.checkedState && myRGB.checkedState) || (myCMYK.checkedState == false && myRGB.checkedState == false)) {
            alert("Please select either CMYK or RGB. You can't create a chart with both.");
            return;
        }

        if (myHoriColMenu.selectedIndex == myVertColMenu.selectedIndex) {
            alert("Please ensure the the horiztonal and vertical adjustments are different.");
            return;
        }
        // get all the values from the UI
        // CMYK data
        var colourModeCMYK = myCMYK.checkedState;
        var cyanVal = cyanValField.editValue;
        var magentaVal = magentaValField.editValue;
        var yellowVal = yellowValField.editValue;
        var blackVal = blackValField.editValue;
        var tacVal = tacValField.editValue;
        //RGB data
        var colourModeRGB = myRGB.checkedState;
        var redVal = redValField.editValue;
        var greenVal = greenValField.editValue;
        var blueVal = blueValField.editValue;
        // Chart Generation Data
        var myHoriCol = myHoriColMenu.selectedIndex;
        var myVertCol = myVertColMenu.selectedIndex;
        var myBoxSize = myBoxSizeField.editValue;
        var myHoriStep = myHoriStepField.selectedIndex;
        var myVertStep = myVertStepField.selectedIndex;
        var myGap = myGapField.editValue;
 
        // Page Data
        var myPageStep = myPageStepField.selectedIndex;
        var myPageWidth_pt = myPageWidthField.editValue;
        var myPageHeight_pt = myPageHeightField.editValue;

        // create the build settings
        var buildsettings = {
            color: null,
            radius: myBoxSize/2,
            gap: myGap,
            max_depth: 999,
            tac: tacVal
        }

        var hsb_range = ["h","s","b"]
        buildsettings["x_axis_adjustment"] = hsb_range[myHoriCol];
        buildsettings["y_axis_adjustment"] = hsb_range[myVertCol];

        buildsettings["z_axis_adjustment"] = "b"; // default to brightness
        if (myHoriCol != 0 && myVertCol != 0) buildsettings["z_axis_adjustment"] = "h";
        if (myHoriCol != 1 && myVertCol != 1) buildsettings["z_axis_adjustment"] = "s";

        var multipler_range = [1,2,5,10,20,30];
        buildsettings["x_axis_multiplier"] = multipler_range[myHoriStep];
        buildsettings["y_axis_multiplier"] = multipler_range[myVertStep];
        buildsettings["z_axis_multiplier"] = multipler_range[myPageStep];

        // create a new document
        var myDocument = app.documents.add();
        with (myDocument) {
            viewPreferences.horizontalMeasurementUnits = MeasurementUnits.MILLIMETERS;
            viewPreferences.verticalMeasurementUnits = MeasurementUnits.MILLIMETERS;
            with (myDocument.documentPreferences) {
                pageHeight = myPageWidth_pt/2.8346438836889;
                pageWidth = myPageHeight_pt/2.8346438836889;
                pageOrientation = PageOrientation.landscape;
                facingPages = false;
            }
            with (myDocument.marginPreferences) {
                top = 0;
                bottom = 0;
                left = 0;
                right = 0;
            }
            var myPage = myDocument.pages.item(0);
            //change to the masterpage and add some notes
            var masterPage = myDocument.masterSpreads.item("A-Master");
            if (!masterPage.isValid) {masterPage = myDocument.masterSpreads.item("A-Parent");}
            var masterText = scriptname + " " + scriptversion + " (Size: " + myBoxSize + "mm, Gap: " + myGap + "mm,";
            masterText += " H=" + buildsettings.x_axis_adjustment + myHoriStep;
            masterText += " V=" + buildsettings.y_axis_adjustment + myVertStep;
            masterText += " S=" + buildsettings.z_axis_adjustment + myPageStep;
            if (colourModeCMYK) {
                masterText += " TAC: " + tacVal + " Start Colour: C" + cyanVal + "% M" + magentaVal + "% Y" + yellowVal + "% K" + blackVal + "%";
                masterText += " Document Profile: " + app.colorSettings.workingSpaceCMYK + ")";
            } else {
                masterText += " Start Colour: R" + redVal + " G" + greenVal + " B" + blueVal;
                masterText += " Document Profile: " + app.colorSettings.workingSpaceRGB + ")";
            }
            var myTextFrame = masterPage.textFrames.add({ geometricBounds: [myPage.bounds[2]-10, myPage.bounds[1], myPage.bounds[2], myPage.bounds[3]-10], contents: masterText });
            var myText = myTextFrame.parentStory.paragraphs.item(0);
            myText.pointSize = 8;
            myText.justification = Justification.RIGHT_ALIGN;
            myDocument.metadataPreferences.author = scriptname + " " + scriptversion;
            myDocument.metadataPreferences.copyrightStatus = CopyrightStatus.NO;
            myDocument.metadataPreferences.copyrightInfoURL = "https://bitbucket.org/nickheaphy/indesign-colour-search-pattern-generator";
            
            // build the search pattern
            if (colourModeCMYK) {
                var initialcolour = createCMYKcolor(myDocument,cyanVal,magentaVal,yellowVal,blackVal);
            } else {
                var initialcolour = createRGBcolor(myDocument,redVal,greenVal,blueVal);
            }
            
            buildsettings.color = initialcolour;
            fillpage_3(myPage, buildsettings);
        }
	} 
	//Remove the dialog box from memory.
	myDialog.destroy();
}



// ----------------------------------------------------------------------------
// Utility Functions

// -----------------------------------------------------------
// -----------------------------------------------------------
/**
 * Create a new colour in the pallete with the applied variations
 * @param {col} original - the original colour
 * @param {int} h_adj - the hue adjustment
 * @param {int} s_adj - the saturation adjustment
 * @param {int} b_adj - the brightness adjustment
 * @returns the new colour
 */
 function createAdjustedColour(myDocument,original,h_adj,s_adj,b_adj) {

    function clamp(n, minn, maxn) {
        return Math.max(Math.min(maxn, n), minn);
    }

    try {
        var newCol = myDocument.colors.item("TEMP");
        myName = newCol.name;
    } catch (myError) {
        var newCol = myDocument.colors.add({name:"TEMP", space: original.space, model:original.model, colorValue:original.colorValue});
    }
    // convert to HSB
    newCol.space = ColorSpace.HSB;
    // Add values
    
    var h = newCol.colorValue[0];
    var s = newCol.colorValue[1];
    var b = newCol.colorValue[2];
    h = (((h+h_adj) % 360) + 360) % 360;
    if (h < 0) h += 360;
    s = clamp(s+s_adj,0,100);
    b = clamp(b+b_adj,0,100);
    newCol.colorValue = [h,s,b]
    // Change back to original colourspace
    newCol.space = original.space
    // round the colours to whole numbers
    if (newCol.space == ColorSpace.CMYK) {
        newCol.colorValue = [Math.round(newCol.colorValue[0]),Math.round(newCol.colorValue[1]),Math.round(newCol.colorValue[2]),Math.round(newCol.colorValue[3])];
        var newname = "C" + newCol.colorValue[0] + " M" + newCol.colorValue[1] + " Y" + newCol.colorValue[2] + " K" + newCol.colorValue[3];
    } else {
        newCol.colorValue = [Math.round(newCol.colorValue[0]),Math.round(newCol.colorValue[1]),Math.round(newCol.colorValue[2])];
        var newname = "R" + newCol.colorValue[0] + " G" + newCol.colorValue[1] + " B" + newCol.colorValue[2];
    }
    // rename
    try {
        newCol.name = newname;
    } catch (myError) {
        // this colour already exists. Delete the new colour and return the existing one.
        newCol.remove();
        newCol = myDocument.colors.item(newname);
    }
    return newCol;
}

// -----------------------------------------------------------
/**
 * Create a CMYK Colour
 * @param {*} c 
 * @param {*} m 
 * @param {*} y 
 * @param {*} k 
 * @returns the colour
 */
function createCMYKcolor(myDocument,c,m,y,k) {
    var colname = "C" + c + " M" + m + " Y" + y + " K" + k;
    try {
        var myColor = myDocument.colors.item(colname);
        //If the color does not exist, trying to get its name will generate an error.
        var myName = myColor.name;
    }
    catch (myError) {
        //The color style did not exist, so create it.
        var myColor = myDocument.colors.add({name:colname, space: ColorSpace.CMYK, model:ColorModel.PROCESS, colorValue:[c, m, y, k]});
        myColor.name = colname;
    }
    return myColor;
}

// -----------------------------------------------------------
/**
 * Create a RGB colour
 * @param {*} r 
 * @param {*} g 
 * @param {*} b 
 * @returns the colour
 */
function createRGBcolor(myDocument,r,g,b) {
    var colname = "R" + r + " G" + g + " B" + b;
    try {
        var myColor = myDocument.colors.item(colname);
        //If the color does not exist, trying to get its name will generate an error.
        var myName = myColor.name;
    }
    catch (myError) {
        //The color style did not exist, so create it.
        var myColor = myDocument.colors.add({name:colname, space: ColorSpace.RGB, model:ColorModel.PROCESS, colorValue:[r, g, b]});
        myColor.name = colname;
    }
    return myColor;
}

// -----------------------------------------------------------
/**
 * Fill the page with three expanding hexagons of varying colours
 * @param {*} myPage - the page object to draw on
 * @param {*} bs - the buildsettings to use to draw
 */
function fillpage_3(myPage, bs) {

    // need to calculate the number of layers required to fill the page
    var w = myPage.bounds[3]/3;
    var boxwidth = 2*bs.radius*Math.cos(30 * Math.PI / 180);
    var layers = Math.floor(Math.floor((w-boxwidth) / (2*bs.radius + bs.gap)) / 2);

    //build and display the progress bar
    progress((layers+1)*3);

    // central hex
    for (var i = 0; i <= layers; i++) {
        progress.message("Drawing Central Grid - Level " + i + " of " + layers);
        polygrid(myPage,i,myPage.bounds[3]/2,myPage.bounds[2]/2,bs);
        progress.increment();
        if (i >= bs.max_depth) break;
    }

    var originalcolor = bs.color;
    var newColor;
    var h=0;
    var s=0;
    var b=0;
    //left hex
    //need to shift the colour
    switch (bs.z_axis_adjustment) {
        case "h":
            h = -1 * bs.z_axis_multiplier/2;
            break;
        case "s":
            s = -1 * bs.z_axis_multiplier/2;
            break;
        default:
            b = -1 * bs.z_axis_multiplier/2;
    }
    var newColor = createAdjustedColour(myPage.parent.parent,originalcolor,h,s,b);
    bs.color = newColor;

    for (var i = 0; i <= layers; i++) {
        progress.message("Drawing Left Grid - Level " + i + " of " + layers);
        polygrid(myPage,i,w/2,myPage.bounds[2]/2,bs);
        progress.increment();
        if (i >= bs.max_depth) break;
    }

    //right hex
    //need to shift the colour
    switch (bs.z_axis_adjustment) {
        case "h":
            h =  bs.z_axis_multiplier/2;
            break;
        case "s":
            s =  bs.z_axis_multiplier/2;
            break;
        default:
            b =  bs.z_axis_multiplier/2;
    }
    newColor = createAdjustedColour(myPage.parent.parent,originalcolor,h,s,b);
    bs.color = newColor;
    for (var i = 0; i <= layers; i++) {
        progress.message("Drawing Right Grid - Level " + i + " of " + layers);
        polygrid(myPage,i,myPage.bounds[3]/2+w,myPage.bounds[2]/2,bs);
        progress.increment();
        if (i >= bs.max_depth) break;
    }

    progress.close();
}

// -----------------------------------------------------------
/**
 * Draw a grid of hexagons around the x,y point based on the depth
 * eg a depth of 0 would draw a single hexagon at x,y
 * a depth of 2 would draw 6 hexagon around a central hexagon (drawn with a depth=0)
 * @param {*} myPage - the page to draw to
 * @param {int} depth - the depth of the hexagon to draw
 * @param {int} x - x postions
 * @param {int} y  - y postions
 * @param {*} bs - the build setting Object
 */
function polygrid(myPage,depth,x,y,bs) {

    function degToRad(degrees) {
        return degrees * (Math.PI / 180);
    };
  
    function radToDeg(rad) {
        return rad / (Math.PI / 180);
    };

    function polar_2_rect(r, theta) {
        var x = r * Math.cos(degToRad(theta));
        var y = r * Math.sin(degToRad(theta));
        return [x,y];
    };

    var num = 6 * depth;

    // central hexagon. No colour adjustment required
    if (depth == 0) {
        drawPolygon(myPage, x, y, bs.radius, bs.color, false, true);
        return;
    }

    for (var i = 0; i < num; i++) {
        // https://www.calculator.net/triangle-calculator.html
        // Calculates b, ∠A, and ∠C based on given a, c, and ∠B.
        var a = 2*bs.radius*depth + bs.gap*depth;
        var c = a/depth * (i % depth);
        var B = 60;
        // b = √a2 + c2 - 2ac·cos(B) 
        var b = Math.sqrt(Math.pow(a,2) + Math.pow(c,2) - 2*a*c * Math.cos(degToRad(B)));
        // ∠C = arccos(b2 + a2 - c2/2ba)
        var C = radToDeg(Math.acos((Math.pow(b,2) + Math.pow(a,2) - Math.pow(c,2)) / (2 * b * a)));
        // Note: math.floor(i/depth)*60 calculates which quadrant we are in
        dxdy = polar_2_rect(b,(Math.floor(i/depth)*60)+C);
        // colour conversions
        var newColor;
        var h=0;
        var s=0;
        var b=0;
        switch (bs.x_axis_adjustment) {
            case "h":
                h = dxdy[0]/bs.radius * bs.x_axis_multiplier/4
                break;
            case "s":
                s = dxdy[0]/bs.radius * bs.x_axis_multiplier/4
                break;
            default:
                b = dxdy[0]/bs.radius * bs.x_axis_multiplier/4
        }
        switch (bs.y_axis_adjustment) {
            case "h":
                h = dxdy[1]/bs.radius * bs.y_axis_multiplier/4
                break;
            case "s":
                s = dxdy[1]/bs.radius * bs.y_axis_multiplier/4
                break;
            default:
                b = dxdy[1]/bs.radius * bs.y_axis_multiplier/4
        }
        newColor = createAdjustedColour(myPage.parent.parent,bs.color,h,s,b)
        //check that TAC not exceeded. Draw an empty hex if it has been...
        if (newColor.space == ColorSpace.CMYK) {
            if ((newColor.colorValue[0]+newColor.colorValue[1]+newColor.colorValue[2]+newColor.colorValue[3]) < bs.tac) {
                drawPolygon(myPage, x+dxdy[0], y+dxdy[1], bs.radius, newColor, false, true);
            } else {
                drawPolygon(myPage, x+dxdy[0], y+dxdy[1], bs.radius, newColor, true, false);
            }
        } else {
            drawPolygon(myPage, x+dxdy[0], y+dxdy[1], bs.radius, newColor, false, true);
        }
    }
}

// -----------------------------------------------------------
/**
 * Show a progress bar onscreen. This has the side effect of updating the screen
 * when the progress bar updates, so the users can see what is happening...
 * @param {int} steps - the steps in the progress bar 
 */
function progress(steps) {
    var b;
    var t;
    var w;
    w = new Window("palette", "Progress", undefined, {closeButton: false});
    t = w.add("statictext");
    t.preferredSize = [450, -1]; // 450 pixels wide, default height.
    if (steps) {
        b = w.add("progressbar", undefined, 0, steps);
        b.preferredSize = [450, -1]; // 450 pixels wide, default height.
    }
    progress.close = function () {
        w.close();
    };
    progress.increment = function () {
        b.value++;
    };
    progress.message = function (message) {
        t.text = message;
    };
    w.show();
}

// -----------------------------------------------------------
/**
 * Draw a Hexagon on the page
 * This code is so ugly :-(
 * @param {*} myPage - myDocument.pages item
 * @param {int} x - the x postion to draw (centre)
 * @param {int} y - the y position to draw (centre)
 * @param {int} radius - the radius of the hexagon
 * @param {color} fill - the fill colour (either CMYK or RGB) from myDocument.colors.item
 * @param {boolean} stroke - do we stroke the hexagon
 * @param {boolean} label - do we label the hexagon
 */
 function drawPolygon(myPage,x,y,radius,fill,stroke,label) {
    
	// draw the polygon. the need to calcualte the bounding box
    var width = 2*radius;
    var height = 2*radius*Math.cos(30 * Math.PI / 180);

    // don't draw if off the page
    if (x-height/2 < 0 || x+height/2 > myPage.bounds[3] || y-radius < 0 || y+radius > myPage.bounds[2]) return;

	var myPolygon = myPage.polygons.add("Layer 1", 6);
	myPolygon.geometricBounds = [y-height/2,x-width/2,y+height/2,x+width/2];
    myPolygon.fillColor = fill;
    myPolygon.fillTint = 100;
    if (stroke) {
        myPolygon.strokeColor = fill;
        myPolygon.strokeWeight = "0.25pt";
        myPolygon.fillTint = 0;
    } else {
        myPolygon.strokeWeight = 0;
    }
    // rotate the hexagon
    var myRotateMatrix = app.transformationMatrices.add({counterclockwiseRotationAngle:30});
    myPolygon.transform(CoordinateSpaces.pasteboardCoordinates, AnchorPoint.centerAnchor, myRotateMatrix);
    // add the text frames
    // rotate them to sit on the edge of the hexagon
    if (label) {
    // first frame - either CM or RG
        var myTextFrame = myPage.textFrames.add();
        myTextFrame.geometricBounds = [y+width/2,x-height/2,y+width/2+3,x];
        if (fill.space == ColorSpace.CMYK) {
            myTextFrame.contents = "C" + fill.colorValue[0] + " M" + fill.colorValue[1];
        } else {
            myTextFrame.contents = "R" + fill.colorValue[0] + " G" + fill.colorValue[1];
        }
        myTextFrame.textFramePreferences.verticalJustification = VerticalJustification.TOP_ALIGN;
        var myText = myTextFrame.parentStory.paragraphs.item(0);
        myText.pointSize = "4pt";
        myText.justification = Justification.CENTER_ALIGN;
        var myRotateMatrix = app.transformationMatrices.add({counterclockwiseRotationAngle:-30});
        myTextFrame.transform(CoordinateSpaces.pasteboardCoordinates, AnchorPoint.topRightAnchor, myRotateMatrix);
        myTextFrame.textFramePreferences.autoSizingReferencePoint = AutoSizingReferenceEnum.TOP_CENTER_POINT;
        myTextFrame.textFramePreferences.autoSizingType = AutoSizingTypeEnum.HEIGHT_AND_WIDTH;
        myTextFrame.textFramePreferences.useNoLineBreaksForAutoSizing = true;
        //second frame - either YK or B
        var myTextFrame = myPage.textFrames.add();
        myTextFrame.geometricBounds = [y+width/2,x,y+width/2+3,x+height/2];
        if (fill.space == ColorSpace.CMYK) {
            myTextFrame.contents = "Y" + fill.colorValue[2] + " K" + fill.colorValue[3];
        } else {
            myTextFrame.contents = "B" + fill.colorValue[2];
        }
        myTextFrame.textFramePreferences.verticalJustification = VerticalJustification.TOP_ALIGN;
        var myText = myTextFrame.parentStory.paragraphs.item(0);
        myText.pointSize = "4pt";
        myText.justification = Justification.CENTER_ALIGN;
        var myRotateMatrix = app.transformationMatrices.add({counterclockwiseRotationAngle:30});
        myTextFrame.transform(CoordinateSpaces.pasteboardCoordinates, AnchorPoint.topLeftAnchor, myRotateMatrix);
        myTextFrame.textFramePreferences.autoSizingReferencePoint = AutoSizingReferenceEnum.TOP_CENTER_POINT;
        myTextFrame.textFramePreferences.autoSizingType = AutoSizingTypeEnum.HEIGHT_AND_WIDTH;
        myTextFrame.textFramePreferences.useNoLineBreaksForAutoSizing = true;
    }
}